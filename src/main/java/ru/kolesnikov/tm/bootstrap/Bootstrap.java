package ru.kolesnikov.tm.bootstrap;

import ru.kolesnikov.tm.api.controller.ICommandController;
import ru.kolesnikov.tm.api.controller.IProjectController;
import ru.kolesnikov.tm.api.controller.ITaskController;
import ru.kolesnikov.tm.api.repository.ICommandRepository;
import ru.kolesnikov.tm.api.repository.IProjectRepository;
import ru.kolesnikov.tm.api.repository.ITaskRepository;
import ru.kolesnikov.tm.api.service.ICommandService;
import ru.kolesnikov.tm.api.service.IProjectService;
import ru.kolesnikov.tm.api.service.ITaskService;
import ru.kolesnikov.tm.constant.ArgumentConst;
import ru.kolesnikov.tm.constant.TerminalConst;
import ru.kolesnikov.tm.controller.CommandController;
import ru.kolesnikov.tm.controller.ProjectController;
import ru.kolesnikov.tm.controller.TaskController;
import ru.kolesnikov.tm.repository.CommandRepository;
import ru.kolesnikov.tm.repository.ProjectRepository;
import ru.kolesnikov.tm.repository.TaskRepository;
import ru.kolesnikov.tm.service.CommandService;
import ru.kolesnikov.tm.service.ProjectService;
import ru.kolesnikov.tm.service.TaskService;
import ru.kolesnikov.tm.util.TerminalUtil;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    public void run(final String[] args) {
        System.out.println("** WELCOME TO TASK MANAGER **");
        if (parseArgs(args)) System.exit(0);
        while (true) parseCommand(TerminalUtil.nextLine());
    }

    private void parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.INFO:
                commandController.showInfo();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case ArgumentConst.COMMANDS:
                commandController.showCommands();
                break;
        }
    }

    private void parseCommand(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.INFO:
                commandController.showInfo();
                break;
            case TerminalConst.EXIT:
                commandController.exit();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTasks();
                break;
            case TerminalConst.TASK_LIST:
                taskController.showTasks();
                break;
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case TerminalConst.TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case TerminalConst.TASK_VIEW_BY_ID:
                taskController.showTaskById();
                break;
            case TerminalConst.TASK_VIEW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case TerminalConst.TASK_VIEW_BY_NAME:
                taskController.showTaskByName();
                break;
            case TerminalConst.TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case TerminalConst.TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case TerminalConst.TASK_REMOVE_BY_NAME:
                taskController.removeTaskByName();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case TerminalConst.PROJECT_VIEW_BY_ID:
                projectController.showProjectById();
                break;
            case TerminalConst.PROJECT_VIEW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case TerminalConst.PROJECT_VIEW_BY_NAME:
                projectController.showProjectByName();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_NAME:
                projectController.removeProjectByName();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showProjects();
                break;
        }
    }

    private boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

}
